/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author e_for
 */
@Entity
@Table(name = "vak")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vak.findAll", query = "SELECT v FROM Vak v"),
    @NamedQuery(name = "Vak.findById", query = "SELECT v FROM Vak v WHERE v.id = :id"),
    @NamedQuery(name = "Vak.findByNaam", query = "SELECT v FROM Vak v WHERE v.naam = :naam")})
public class Vak implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAAM")
    private String naam;
    @ManyToMany(mappedBy = "vakCollection")
    private Collection<Student> studentCollection;
    @OneToMany(mappedBy = "vakId")
    private Collection<Test> testCollection;
    @OneToMany(mappedBy = "vakId")
    private Collection<VakLeerkracht> vakLeerkrachtCollection;

    public Vak() {
    }

    public Vak(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    @XmlTransient
    public Collection<Student> getStudentCollection() {
        return studentCollection;
    }

    public void setStudentCollection(Collection<Student> studentCollection) {
        this.studentCollection = studentCollection;
    }

    @XmlTransient
    public Collection<Test> getTestCollection() {
        return testCollection;
    }

    public void setTestCollection(Collection<Test> testCollection) {
        this.testCollection = testCollection;
    }

    @XmlTransient
    public Collection<VakLeerkracht> getVakLeerkrachtCollection() {
        return vakLeerkrachtCollection;
    }

    public void setVakLeerkrachtCollection(Collection<VakLeerkracht> vakLeerkrachtCollection) {
        this.vakLeerkrachtCollection = vakLeerkrachtCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vak)) {
            return false;
        }
        Vak other = (Vak) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Vak[ id=" + id + " ]";
    }

}
