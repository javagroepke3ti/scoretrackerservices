/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author e_for
 */
@Entity
@Table(name = "test")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Test.findAll", query = "SELECT t FROM Test t"),
    @NamedQuery(name = "Test.findById", query = "SELECT t FROM Test t WHERE t.id = :id"),
    @NamedQuery(name = "Test.findByNaam", query = "SELECT t FROM Test t WHERE t.naam = :naam"),
    @NamedQuery(name = "Test.findByNaamEnVak", query = "SELECT t FROM Test t WHERE t.naam = :naam and t.vakId.naam = :vaknaam")})
public class Test implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAAM")
    private String naam;
    @ManyToMany
    @JoinTable(name="klas_test")
    private Collection<Klas> klasCollection;
    @JoinColumn(name = "VAK_ID", referencedColumnName = "ID")
    @ManyToOne
    private Vak vakId;
    @OneToMany(mappedBy = "testId")
    private Collection<StudentTest> studentTestCollection;

    public Test() {
    }

    public Test(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    @XmlTransient
    public Collection<Klas> getKlasCollection() {
        return klasCollection;
    }

    public void setKlasCollection(Collection<Klas> klasCollection) {
        this.klasCollection = klasCollection;
    }

    public Vak getVakId() {
        return vakId;
    }

    public void setVakId(Vak vakId) {
        this.vakId = vakId;
    }

    @XmlTransient
    public Collection<StudentTest> getStudentTestCollection() {
        return studentTestCollection;
    }

    public void setStudentTestCollection(Collection<StudentTest> studentTestCollection) {
        this.studentTestCollection = studentTestCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Test)) {
            return false;
        }
        Test other = (Test) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Test[ id=" + id + " ]";
    }

}
