/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author e_for
 */
@Entity
@Table(name = "vak_leerkracht")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VakLeerkracht.findAll", query = "SELECT v FROM VakLeerkracht v"),
    @NamedQuery(name = "VakLeerkracht.findById", query = "SELECT v FROM VakLeerkracht v WHERE v.id = :id")})
public class VakLeerkracht implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @ManyToMany
    @JoinTable(name="vakLeerkracht_klas")
    private Collection<Klas> klasCollection;
    @JoinColumn(name = "LEERKRACHT_ID", referencedColumnName = "ID")
    @ManyToOne
    private Leerkracht leerkrachtId;
    @JoinColumn(name = "VAK_ID", referencedColumnName = "ID")
    @ManyToOne
    private Vak vakId;

    public VakLeerkracht() {
    }

    public VakLeerkracht(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<Klas> getKlasCollection() {
        return klasCollection;
    }

    public void setKlasCollection(Collection<Klas> klasCollection) {
        this.klasCollection = klasCollection;
    }

    public Leerkracht getLeerkrachtId() {
        return leerkrachtId;
    }

    public void setLeerkrachtId(Leerkracht leerkrachtId) {
        this.leerkrachtId = leerkrachtId;
    }

    public Vak getVakId() {
        return vakId;
    }

    public void setVakId(Vak vakId) {
        this.vakId = vakId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VakLeerkracht)) {
            return false;
        }
        VakLeerkracht other = (VakLeerkracht) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.VakLeerkracht[ id=" + id + " ]";
    }

}
