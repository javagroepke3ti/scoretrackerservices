/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author e_for
 */
@Entity
@Table(name = "leerkracht")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Leerkracht.findAll", query = "SELECT l FROM Leerkracht l"),
    @NamedQuery(name = "Leerkracht.findById", query = "SELECT l FROM Leerkracht l WHERE l.id = :id"),
    @NamedQuery(name = "Leerkracht.findByGebruikersnaam", query = "SELECT l FROM Leerkracht l WHERE l.gebruikersnaam = :gebruikersnaam"),
    @NamedQuery(name = "Leerkracht.findByNaam", query = "SELECT l FROM Leerkracht l WHERE l.naam = :naam"),
    @NamedQuery(name = "Leerkracht.findByVoornaam", query = "SELECT l FROM Leerkracht l WHERE l.voornaam = :voornaam"),
    @NamedQuery(name = "Leerkracht.findByWachtwoord", query = "SELECT l FROM Leerkracht l WHERE l.wachtwoord = :wachtwoord")})
public class Leerkracht implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "GEBRUIKERSNAAM")
    private String gebruikersnaam;
    @Size(max = 255)
    @Column(name = "NAAM")
    private String naam;
    @Size(max = 255)
    @Column(name = "VOORNAAM")
    private String voornaam;
    @Size(max = 255)
    @Column(name = "WACHTWOORD")
    private String wachtwoord;
    @OneToMany(mappedBy = "leerkrachtId")
    private Collection<VakLeerkracht> vakLeerkrachtCollection;

    public Leerkracht() {
    }

    public Leerkracht(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGebruikersnaam() {
        return gebruikersnaam;
    }

    public void setGebruikersnaam(String gebruikersnaam) {
        this.gebruikersnaam = gebruikersnaam;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getWachtwoord() {
        return wachtwoord;
    }

    public void setWachtwoord(String wachtwoord) {
        this.wachtwoord = wachtwoord;
    }

    @XmlTransient
    public Collection<VakLeerkracht> getVakLeerkrachtCollection() {
        return vakLeerkrachtCollection;
    }

    public void setVakLeerkrachtCollection(Collection<VakLeerkracht> vakLeerkrachtCollection) {
        this.vakLeerkrachtCollection = vakLeerkrachtCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Leerkracht)) {
            return false;
        }
        Leerkracht other = (Leerkracht) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Leerkracht[ id=" + id + " ]";
    }

}
