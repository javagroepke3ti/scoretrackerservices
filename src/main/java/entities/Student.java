/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author e_for
 */
@Entity
@Table(name = "student")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s"),
    @NamedQuery(name = "Student.findById", query = "SELECT s FROM Student s WHERE s.id = :id"),
    @NamedQuery(name = "Student.findByEmail", query = "SELECT s FROM Student s WHERE s.email = :email"),
    @NamedQuery(name = "Student.findByNaam", query = "SELECT s FROM Student s WHERE s.naam = :naam"),
    @NamedQuery(name = "Student.findByStudentnummer", query = "SELECT s FROM Student s WHERE s.studentnummer = :studentnummer"),
    @NamedQuery(name = "Student.findByVoornaam", query = "SELECT s FROM Student s WHERE s.voornaam = :voornaam")})
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "EMAIL")
    private String email;
    @Size(max = 255)
    @Column(name = "NAAM")
    private String naam;
    @Size(max = 255)
    @Column(name = "STUDENTNUMMER")
    private String studentnummer;
    @Size(max = 255)
    @Column(name = "VOORNAAM")
    private String voornaam;
    @JoinTable(name = "student_vak", joinColumns = {
        @JoinColumn(name = "studenten_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "vakken_ID", referencedColumnName = "ID")})
    @ManyToMany
    private Collection<Vak> vakCollection;
    @JoinColumn(name = "KLAS_ID", referencedColumnName = "ID")
    @ManyToOne
    private Klas klasId;
    @OneToMany(mappedBy = "studentId")
    private Collection<StudentTest> studentTestCollection;

    public Student() {
    }

    public Student(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getStudentnummer() {
        return studentnummer;
    }

    public void setStudentnummer(String studentnummer) {
        this.studentnummer = studentnummer;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    @XmlTransient
    public Collection<Vak> getVakCollection() {
        return vakCollection;
    }

    public void setVakCollection(Collection<Vak> vakCollection) {
        this.vakCollection = vakCollection;
    }

    public Klas getKlasId() {
        return klasId;
    }

    public void setKlasId(Klas klasId) {
        this.klasId = klasId;
    }

    @XmlTransient
    public Collection<StudentTest> getStudentTestCollection() {
        return studentTestCollection;
    }

    public void setStudentTestCollection(Collection<StudentTest> studentTestCollection) {
        this.studentTestCollection = studentTestCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Student[ id=" + id + " ]";
    }

}
