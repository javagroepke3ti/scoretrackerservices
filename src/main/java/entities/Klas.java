/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author e_for
 */
@Entity
@Table(name = "klas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Klas.findAll", query = "SELECT k FROM Klas k"),
    @NamedQuery(name = "Klas.findById", query = "SELECT k FROM Klas k WHERE k.id = :id"),
    @NamedQuery(name = "Klas.findByNaam", query = "SELECT k FROM Klas k WHERE k.naam = :naam")})
public class Klas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAAM")
    private String naam;
    @ManyToMany(mappedBy="klasCollection")
    private Collection<VakLeerkracht> vakLeerkrachtCollection;
    @ManyToMany(mappedBy = "klasCollection")
    private Collection<Test> testCollection;
    @OneToMany(mappedBy = "klasId")
    private Collection<Student> studentCollection;

    public Klas() {
    }

    public Klas(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    @XmlTransient
    public Collection<VakLeerkracht> getVakLeerkrachtCollection() {
        return vakLeerkrachtCollection;
    }

    public void setVakLeerkrachtCollection(Collection<VakLeerkracht> vakLeerkrachtCollection) {
        this.vakLeerkrachtCollection = vakLeerkrachtCollection;
    }

    @XmlTransient
    public Collection<Test> getTestCollection() {
        return testCollection;
    }

    public void setTestCollection(Collection<Test> testCollection) {
        this.testCollection = testCollection;
    }

    @XmlTransient
    public Collection<Student> getStudentCollection() {
        return studentCollection;
    }

    public void setStudentCollection(Collection<Student> studentCollection) {
        this.studentCollection = studentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Klas)) {
            return false;
        }
        Klas other = (Klas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Klas[ id=" + id + " ]";
    }
    
}
