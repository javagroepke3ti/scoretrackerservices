/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excel;

import entities.Student;
import entities.Klas;
import entities.StudentTest;
import entities.Test;
import entities.Vak;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import services.KlasFacadeREST;
import services.VakFacadeREST;
import services.StudentFacadeREST;
import services.StudentTestFacadeREST;
import services.TestFacadeREST;

/**
 *
 * @author e_for
 */
public class ExcelReaderImpl implements ExcelReader {

    @Override
    public String readStudentsKlasAndSave(InputStream file, StudentFacadeREST studentService, KlasFacadeREST klasService) {
        List<Student> studenten = new ArrayList<>();
        Klas klas = new Klas();
        String resultaat = "";
        
        try {
            //Get the workbook instance for XLS file 
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int teller = 0;

            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();

                if (teller == 0) {
                    Iterator<Cell> cellIterator = row.cellIterator();

                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        //Checken in welke kolom de Iterator staat
                        int columnIndex = cell.getColumnIndex();

                        switch (columnIndex) {
                            case 1:
                                //Kijken of de klas al bestaat en zo niet, fout doorsturen
                                List<Klas>alleKlassen = klasService.findAll();
                                List<String>klasnamen = new ArrayList<>();
                                for (Klas k : alleKlassen)
                                {
                                    klasnamen.add(k.getNaam());
                                }    
                                
                                if (klasnamen.contains(cell.getStringCellValue()))
                                {
                                    return "klasBestaatAl";                                  
                                }
                                else
                                {
                                    klas.setNaam(cell.getStringCellValue());
                                }  
                                break;
                        }
                    }

                }

                if (teller >= 2) {

                    Student student = new Student();
                    //For each row, iterate through each columns
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        //Checken in welke kolom de Iterator staat
                        int columnIndex = cell.getColumnIndex();

                        switch (columnIndex) {
                            //Indien in de eerste kolom, moet de waarde in het attribuut StudentNummer
                            case 0:
                                student.setStudentnummer(cell.getStringCellValue());
                                break;
                            //Indien in de tweede kolom, moet de waarde in het attribuut Naam
                            case 1:
                                student.setNaam(cell.getStringCellValue());
                                break;
                            //Indien in de derde kolom, moet de waarde in het attribuut Voornaam
                            case 2:
                                student.setVoornaam(cell.getStringCellValue());
                                break;
                            //Indien in de vierde kolom, moet de waarde in het attribuut Email
                            case 3:
                                student.setEmail(cell.getStringCellValue());
                                break;
                        }
                    }
                    //Het object student, dat nu opgevuld is, toevoegen aan de arrayList
                    studenten.add(student);
                }
                teller++;
            }

            klasService.create(klas);

            for (Student student : studenten) {
                student.setKlasId(klas);
            }
            for (Student student : studenten) {
                studentService.create(student);
            }
            
            resultaat = "succes";
            workbook.close();
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(ExcelReaderImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultaat;
    }
    //

    @Override
    public String readTest(InputStream file, StudentFacadeREST studentService, VakFacadeREST vakService, KlasFacadeREST klasService, TestFacadeREST testService, StudentTestFacadeREST studentTestService){
        Test test = new Test();
        Test nieuweTest = null;
        Test bestaandeTest = null;
        List<Test>bestaandeTesten = new ArrayList<>();
        Long testId = null;
        Date date = null;
        Vak vak = new Vak();
        String resultaat = "";
        Boolean testBestaatAl = false;
        String testNaam = "";
        int tellerBestaandeTesten = 0;
        
        try     {
            //Get the workbook instance for XLS file 
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int teller = -1;
            
            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next(); 
                teller++; 
                
                if (teller < 4)
                {
                    Klas klas = new Klas();
                    List<Klas>klassen = new ArrayList<>();
                                        
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        int columnIndex = cell.getColumnIndex();
                        
                        if (teller == 0)
                        {
                            if (columnIndex == 1)
                            {
                                //Kijken of de test al bestaat
                                List<Test>alleTesten = testService.findAll();
                                List<String>testnamen = new ArrayList<>();
                                for (Test t : alleTesten)
                                {
                                    testnamen.add(t.getNaam());
                                }
                                
                                if(testnamen.contains(cell.getStringCellValue()))
                                {
                                    testBestaatAl = true;
                                    bestaandeTesten = testService.findTestByNaam(cell.getStringCellValue());
                                    testNaam = cell.getStringCellValue();
                                }
                                
                                test.setNaam(cell.getStringCellValue());
                            }
                                
                        }
                        
                        if (teller == 1)
                        {
                            if (columnIndex == 1)
                            {
                                //Kijken of het vak al bestaat en zo niet, vak aanmaken
                                List<Vak>alleVakken = vakService.findAll();
                                List<String>vaknamen = new ArrayList<>();
                                for (Vak v : alleVakken)
                                {
                                    vaknamen.add(v.getNaam());
                                }
                                
                                if (vaknamen.contains(cell.getStringCellValue()))
                                {
                                    test.setVakId(vakService.findVakByNaam(cell.getStringCellValue()));
                                    if (testBestaatAl)
                                    {
                                        for (Test t : bestaandeTesten)
                                        {
                                            if (cell.getStringCellValue().equals(t.getVakId().getNaam()))
                                            {
                                                bestaandeTest = testService.findTestByNaamEnVak(testNaam, cell.getStringCellValue());
                                                tellerBestaandeTesten++;
                                            }
                                        }
                                        
                                        if (tellerBestaandeTesten == 0)
                                        {
                                            testBestaatAl = false;
                                        }
                                    }
                                }
                                else
                                {
                                    vak.setNaam(cell.getStringCellValue());
                                    Long vakId = vakService.createWithId(vak);
                                    test.setVakId(vakService.find(vakId));
                                    testBestaatAl = false;
                                }
                            }
                        }
                        
                        if (teller == 2)
                        {
                            if (columnIndex == 1)
                            {
                                //Kijken of de klas al bestaat en zo niet, fout doorsturen
                                List<Klas>alleKlassen = klasService.findAll();
                                List<String>klasnamen = new ArrayList<>();
                                for (Klas k : alleKlassen)
                                {
                                    klasnamen.add(k.getNaam());
                                }    
                                
                                if (klasnamen.contains(cell.getStringCellValue()))
                                {
                                    klas = klasService.findKlasByNaam(cell.getStringCellValue());
                                    
                                    if (testBestaatAl)
                                    {
                                       Collection<Klas>klasCollection = bestaandeTest.getKlasCollection(); 
                                       if (klasCollection.contains(klas))
                                       {
                                           return "testBestaatAl";
                                       }
                                       else
                                       {
                                           klasCollection.add(klas);
                                           testId = testService.updateWithId(bestaandeTest);
                                       }
                                    }
                                    else
                                    {
                                        klassen.add(klas);
                                        test.setKlasCollection(klassen);
                                        testId = testService.createWithId(test);
                                    }                                  
                                }
                                else
                                {
                                    return "klasBestaatNiet";
                                }                             
                            }
                        }
                        
                        if (teller == 3)
                        {
                            if (columnIndex == 1)
                            {
                                date = cell.getDateCellValue(); 
                            }
                        }
                    }
                }
                
                if (teller > 4)
                {
                    StudentTest studenttest = new StudentTest();

                    Iterator<Cell> cellIterator2 = row.cellIterator();
                    while (cellIterator2.hasNext()) {
                        Cell cell = cellIterator2.next();

                        int columnIndex = cell.getColumnIndex();

                        switch (columnIndex) {
                            case 0:
                                studenttest.setDatum(date);
                                nieuweTest = testService.find(testId);
                                studenttest.setTestId(nieuweTest);  
                                List<Student>studenten = studentService.findAll();
                                List<String>nummers = new ArrayList<>();
                                for (Student s : studenten)
                                {
                                    nummers.add(s.getStudentnummer());
                                }
                                
                                if (!nummers.contains(cell.getStringCellValue()))
                                {
                                    return "foutStudent";
                                }
                                else
                                {
                                    Student student = studentService.findStudentByNummer(cell.getStringCellValue());
                                    studenttest.setStudentId(student);
                                }
                                                             
                                break;
                            case 1:
                                break;
                            case 2:
                                String score = cell.getStringCellValue();
                                studenttest.setScore(Double.parseDouble(score));
                                Long id = studentTestService.createWithId(studenttest);
                                break;
                        }
                    }
                }
            }
        workbook.close();
        file.close();
        resultaat = "succes";
        
    }   catch (IOException ex) {
            Logger.getLogger(ExcelReaderImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return resultaat;
    }
}
