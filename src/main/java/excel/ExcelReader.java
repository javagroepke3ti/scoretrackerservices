/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excel;

import java.io.InputStream;
import services.KlasFacadeREST;
import services.StudentFacadeREST;
import services.StudentTestFacadeREST;
import services.TestFacadeREST;
import services.VakFacadeREST;

/**
 *
 * @author Thomas
 */
public interface ExcelReader {

    public String readStudentsKlasAndSave(InputStream file,StudentFacadeREST studentService, KlasFacadeREST klasService);
    public String readTest(InputStream file, StudentFacadeREST studentService, VakFacadeREST vakService, KlasFacadeREST klasService, TestFacadeREST testService, StudentTestFacadeREST studentTestService);
}
