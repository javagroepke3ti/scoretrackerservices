
package pdf;

import entities.Klas;
import entities.Student;
import entities.Test;
import java.io.IOException;
import java.text.ParseException;
import javax.ws.rs.core.Response;
import services.EmailService;
import services.StudentTestFacadeREST;


public interface PdfCreator {
    
    public Response createPdfWithDates(Student student, String startDatum, String eindDatum, StudentTestFacadeREST studentTestService) throws IOException, ParseException;
    public void createPdfWithDatesForStudent(Student student, String startDatum, String eindDatum, StudentTestFacadeREST studentTestService, EmailService mailService) throws IOException, ParseException;
    public Response createPdf(Klas klas, Test test, StudentTestFacadeREST studentTestService) throws IOException;
}
