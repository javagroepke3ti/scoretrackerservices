
package pdf;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import entities.Klas;
import entities.Student;
import entities.StudentTest;
import entities.Test;
import entities.Vak;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.ws.rs.core.Response;
import services.EmailService;
import services.StudentTestFacadeREST;


public class PdfCreatorImpl implements PdfCreator {
    
    @Override
    public Response createPdf(Klas klas, Test gekozenTest, StudentTestFacadeREST studentTestService) throws IOException {
        DecimalFormat df = new DecimalFormat("0.00");
        
        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        
        double klasGemiddelde = 0;
        double[] scores;
        double mediaan;
        int teller = 0;
        
        File file = new File("test.pdf");
        FileOutputStream out = new FileOutputStream(file);

        //Initialize PDF document
        try (PdfWriter writer = new PdfWriter(out)) {
            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(writer);
            
            //Document aanmaken
            Document document = new Document(pdf);
            {
                document.add(new Paragraph(
                        "Klas: " + klas.getNaam() + "\n"
                                + "Vak: " + gekozenTest.getVakId().getNaam() + "\n"
                                + "Onderwerp: " + gekozenTest.getNaam() + "\n"
                ));
                
                Table table = new Table(new float[]{4, 4});
                table.setBorderTop(new SolidBorder(1));
                table.setBorderBottom(new SolidBorder(1));
                table.setBorderLeft(new SolidBorder(1));
                table.setBorderRight(new SolidBorder(1));
                process(table, "Leerling;Punten;", bold, Color.LIGHT_GRAY);
                table.setWidthPercent(100);
                document.add(new Paragraph());
                Collection<StudentTest> studentTesten = studentTestService.findAllPerKlasPerTest(klas.getId(), gekozenTest.getId());
                scores = new double[klas.getStudentCollection().size()];
                
                for (StudentTest studentTest : studentTesten) {
                    if (studentTest.getStudentId().getKlasId().getNaam().equals(klas.getNaam())) {
                        scores[teller] = studentTest.getScore();
                        klasGemiddelde += studentTest.getScore();
                        if (teller % 2 == 0) {
                            process(table, studentTest.getStudentId().getVoornaam() + " " + studentTest.getStudentId().getNaam() + ";" + df.format(studentTest.getScore()) + "%", font, Color.CYAN);
                        } else {
                            process(table, studentTest.getStudentId().getVoornaam() + " " + studentTest.getStudentId().getNaam() + ";" + df.format(studentTest.getScore()) + "%", font, Color.WHITE);
                        }
                        teller += 1;
                    }
                }
                klasGemiddelde /= scores.length;
                Arrays.sort(scores);
                mediaan = scores[scores.length / 2];
                document.add(new Paragraph("Gemiddelde: " + df.format(klasGemiddelde) + "%" + "\n" + "Mediaan: " + df.format(mediaan) + "%"));
                
                document.add(table);
                document.close();
            }
            writer.flush();
            writer.close();
        }
        Response.ResponseBuilder response = Response.ok((Object) file);
        
        response.header("Content-Disposition", "attachment; filename=TestRapport.pdf");
        return response.build();
    }
    
    @Override
    public Response createPdfWithDates(Student student, String startDatum, String eindDatum, StudentTestFacadeREST studentTestService) throws IOException, ParseException
    {
        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        
        DateFormat formatter; 
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = (java.util.Date)formatter.parse(startDatum); 
        Date date2 = (java.util.Date)formatter.parse(eindDatum);

        Collection<Vak>vakken = student.getVakCollection();
        
        //PDF aanmaken
        File file = new File("test.pdf");
        FileOutputStream out = new FileOutputStream(file);

        //Initialize PDF document
        try (PdfWriter writer = new PdfWriter(out)) {
            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(writer);
            
            //Document aanmaken
            Document document = new Document(pdf);
            {
                document.add(new Paragraph(
                        "Rapport: " + startDatum + " - " + eindDatum + "\n"
                                + "Naam: " + student.getVoornaam() + " " + student.getNaam() + "\n"
                                + "Klas: " + student.getKlasId().getNaam() + "\n"
                ));
                
                Table table = new Table(new float[]{4, 4, 3, 3});
                table.setBorderTop(new SolidBorder(1));
                table.setBorderBottom(new SolidBorder(1));
                table.setBorderLeft(new SolidBorder(1));
                table.setBorderRight(new SolidBorder(1));
                process(table, "Vak;Punten;Klasgemiddelde; Mediaan", bold, Color.LIGHT_GRAY);
                table.setWidthPercent(100);
                document.add(new Paragraph());
                
                int teller = 0;
                
                for(Vak vak: vakken)
                {
                    teller++;
                    List<StudentTest>testenPerVak = studentTestService.findAllTestsPerStudentByVakAndDate(student.getId(), vak.getId(), date1, date2);
                    
                    if (testenPerVak.size() > 0)
                    {
                        //Score student berekenen
                        double totaleScore = 0.0;
                        double scoreStudent;
                        
                        for (StudentTest test : testenPerVak)
                        {
                            totaleScore += test.getScore();
                        }
                        scoreStudent = totaleScore / testenPerVak.size();
                        
                        //Gemiddelde en mediaan van de klas berekenen
                        List<StudentTest>testenPerKlas = studentTestService.findAllTestsFromKlasByVakAndDate(student.getKlasId().getId(), vak.getId(), date1, date2);
                        List<Double>scores = new ArrayList<>();
                        
                        double totaleScoreKlas = 0.0;
                        double gemiddeldeKlas;
                        
                        for (StudentTest testKlas : testenPerKlas)
                        {
                            totaleScoreKlas += testKlas.getScore();
                            scores.add(testKlas.getScore());
                        }
                        
                        gemiddeldeKlas = totaleScoreKlas / testenPerKlas.size();
                        
                        Collections.sort(scores);
                        int lengte = scores.size();
                        double mediaan = scores.get(lengte/2);
                        
                        if (teller % 2 == 0)
                        {
                            process(table, vak.getNaam() + ";" + String.format("%.2f", scoreStudent) + "%;" + String.format("%.2f", gemiddeldeKlas) + "%;" + String.format("%.2f", mediaan) + "%", font, Color.WHITE);
                        }
                        else
                        {
                            process(table, vak.getNaam() + ";" + String.format("%.2f", scoreStudent) + "%;" + String.format("%.2f", gemiddeldeKlas) + "%;" + String.format("%.2f", mediaan) + "%", font, Color.CYAN);
                        }
                        
                    }
                }
                document.add(table);
                document.close();
            }
            writer.flush();
            writer.close();
        }
        
        Response.ResponseBuilder response = Response.ok((Object) file);
        
        response.header("Content-Disposition", "attachment; filename=Rapport.pdf");
        return response.build();
    }
    
    @Override
    public void createPdfWithDatesForStudent(Student student, String startDatum, String eindDatum, StudentTestFacadeREST studentTestService, EmailService mailService) throws IOException, ParseException
    {
        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        
        DateFormat formatter; 
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = (java.util.Date)formatter.parse(startDatum); 
        Date date2 = (java.util.Date)formatter.parse(eindDatum);

        //Nadien ophalen van zijn/haar vakken
        Collection<Vak>vakken = student.getVakCollection();
        
        //PDF aanmaken
        File file = new File("test.pdf");
        FileOutputStream out = new FileOutputStream(file);

        //Initialize PDF document
        try (PdfWriter writer = new PdfWriter(out)) {
            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(writer);
            
            //Document aanmaken
            Document document = new Document(pdf);
            {
                document.add(new Paragraph(
                        "Rapport: " + startDatum + " - " + eindDatum + "\n"
                                + "Naam: " + student.getVoornaam() + " " + student.getNaam() + "\n"
                                + "Klas: " + student.getKlasId().getNaam() + "\n"
                ));
                
                Table table = new Table(new float[]{4, 4, 3, 3});
                table.setBorderTop(new SolidBorder(1));
                table.setBorderBottom(new SolidBorder(1));
                table.setBorderLeft(new SolidBorder(1));
                table.setBorderRight(new SolidBorder(1));
                process(table, "Vak;Punten;Klasgemiddelde; Mediaan", bold, Color.LIGHT_GRAY);
                table.setWidthPercent(100);
                document.add(new Paragraph());
                
                int teller = 0;
                
                for(Vak vak: vakken)
                {
                    teller++;
                    List<StudentTest>testenPerVak = studentTestService.findAllTestsPerStudentByVakAndDate(student.getId(), vak.getId(), date1, date2);
                    
                    if (testenPerVak.size() > 0)
                    {
                        //Score student berekenen
                        double totaleScore = 0.0;
                        double scoreStudent;
                        
                        for (StudentTest test : testenPerVak)
                        {
                            totaleScore += test.getScore();
                        }
                        scoreStudent = totaleScore / testenPerVak.size();
                        
                        //Gemiddelde en mediaan van de klas berekenen
                        List<StudentTest>testenPerKlas = studentTestService.findAllTestsFromKlasByVakAndDate(student.getKlasId().getId(), vak.getId(), date1, date2);
                        List<Double>scores = new ArrayList<>();
                        
                        double totaleScoreKlas = 0.0;
                        double gemiddeldeKlas;
                        
                        for (StudentTest testKlas : testenPerKlas)
                        {
                            totaleScoreKlas += testKlas.getScore();
                            scores.add(testKlas.getScore());
                        }
                        
                        gemiddeldeKlas = totaleScoreKlas / testenPerKlas.size();
                        
                        Collections.sort(scores);
                        int lengte = scores.size();
                        double mediaan = scores.get(lengte/2);
                        
                        if (teller % 2 == 0)
                        {
                            process(table, vak.getNaam() + ";" + String.format("%.2f", scoreStudent) + "%;" + String.format("%.2f", gemiddeldeKlas) + "%;" + String.format("%.2f", mediaan) + "%", font, Color.WHITE);
                        }
                        else
                        {
                            process(table, vak.getNaam() + ";" + String.format("%.2f", scoreStudent) + "%;" + String.format("%.2f", gemiddeldeKlas) + "%;" + String.format("%.2f", mediaan) + "%", font, Color.CYAN);
                        }
                        
                    }
                }
                document.add(table);
                document.close();
            }
            writer.flush();
            writer.close();
        }
        
        FileInputStream fileInputStream;
        File fileSaved = new File("test.pdf");
        fileInputStream = new FileInputStream(fileSaved);
        mailService.sendMail(fileInputStream, student.getId());
    }
    
    public void process(Table table, String line, PdfFont font, Color backgroundColor) {
        StringTokenizer tokenizer = new StringTokenizer(line, ";");
        while (tokenizer.hasMoreTokens()) {

            table.addCell(
                    new Cell().add(
                            new Paragraph(tokenizer.nextToken()).setFont(font).setMarginLeft(1)).setBorder(Border.NO_BORDER).setPadding(0.5f).setPaddingRight(0.5f).setBorderLeft(new SolidBorder(1)).setBackgroundColor(backgroundColor));

        }
    }
}
