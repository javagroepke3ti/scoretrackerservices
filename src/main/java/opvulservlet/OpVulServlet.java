/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opvulservlet;

import entities.Klas;
import entities.Leerkracht;
import entities.Student;
import entities.StudentTest;
import entities.Test;
import entities.Vak;
import entities.VakLeerkracht;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import services.KlasFacadeREST;
import services.LeerkrachtFacadeREST;
import services.StudentFacadeREST;
import services.StudentTestFacadeREST;
import services.TestFacadeREST;
import services.VakFacadeREST;
import services.VakLeerkrachtFacadeREST;

/**
 *
 * @author e_for
 */
@WebServlet(name = "OpVulServlet", urlPatterns = {"/OpVulServlet"})
public class OpVulServlet extends HttpServlet {

    @EJB
    private KlasFacadeREST klasService;
    @EJB
    private StudentFacadeREST studentService;
    @EJB
    private VakFacadeREST vakService;
    @EJB
    private LeerkrachtFacadeREST leerkrachtService;
    @EJB
    private TestFacadeREST testService;
    @EJB
    private StudentTestFacadeREST studentTestService;
    @EJB
    private VakLeerkrachtFacadeREST vakLeerkrachtService;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            List<Klas>klassen = new ArrayList<>();        
            Klas klas = new Klas();
            klas.setNaam("1A");        
            Klas klas2 = new Klas();
            klas2.setNaam("1B");        
            klassen.add(klas);
            klassen.add(klas2);        
            klasService.saveKlassen(klassen);

            List<Leerkracht>leerkrachten = new ArrayList<>();        
            Leerkracht lk = new Leerkracht();
            lk.setNaam("Kelly");
            lk.setVoornaam("Gene");
            lk.setGebruikersnaam("GKelly");
            lk.setWachtwoord("rain");   
            Leerkracht lk2 = new Leerkracht();
            lk2.setNaam("Sinatra");
            lk2.setVoornaam("Frank");
            lk2.setGebruikersnaam("FrankieBoy");
            lk2.setWachtwoord("newyork");        
            Leerkracht lk3 = new Leerkracht();
            lk3.setNaam("Simone");
            lk3.setVoornaam("Nina");
            lk3.setGebruikersnaam("Ninanina");
            lk3.setWachtwoord("raycharles");        
            leerkrachten.add(lk);
            leerkrachten.add(lk2);
            leerkrachten.add(lk3);        
            leerkrachtService.saveLeerkrachten(leerkrachten);

            List<Vak>vakken = new ArrayList<>();        
            Vak vak = new Vak();
            vak.setNaam("Linux Advanced");        
            Vak vak2 = new Vak();
            vak2.setNaam("Applicatie-ontwikkeling");        
            Vak vak3 = new Vak();
            vak3.setNaam("Business-project");        
            vakken.add(vak);
            vakken.add(vak2);
            vakken.add(vak3);        
            vakService.saveVakken(vakken); 

            List<Test>testen = new ArrayList<>();        
            Test test = new Test();
            test.setNaam("Tutorial3");
            test.setVakId(vak);
            List<Klas>klassen1 = new ArrayList<>();
            klassen1.add(klas);
            test.setKlasCollection(klassen1);        
            Test test2 = new Test();
            test2.setNaam("Database");
            test2.setVakId(vak);        
            test2.setKlasCollection(klassen1);        
            Test test3 = new Test();
            test3.setNaam("BubbleShooter");
            test3.setVakId(vak2);        
            test3.setKlasCollection(klassen);        
            Test test4 = new Test();
            test4.setNaam("PBS");
            test4.setVakId(vak3);
            List<Klas>klassen2 = new ArrayList<>();
            klassen2.add(klas2);
            test4.setKlasCollection(klassen2);   
            Test test5 = new Test();
            test5.setNaam("Space Invaders");
            test5.setVakId(vak2);
            test5.setKlasCollection(klassen);
            testen.add(test);
            testen.add(test2);
            testen.add(test3);
            testen.add(test4);
            testen.add(test5);
            testService.saveTesten(testen);

            List<VakLeerkracht> vakLeerkrachten = new ArrayList<>();        
            VakLeerkracht vl = new VakLeerkracht();
            vl.setLeerkrachtId(lk);
            vl.setVakId(vak);
            vl.setKlasCollection(klassen1);        
            VakLeerkracht vl2 = new VakLeerkracht();
            vl2.setLeerkrachtId(lk2);
            vl2.setVakId(vak2);
            vl2.setKlasCollection(klassen2);        
            VakLeerkracht vl3 = new VakLeerkracht();
            vl3.setLeerkrachtId(lk3);
            vl3.setVakId(vak3);
            vl3.setKlasCollection(klassen2);        
            VakLeerkracht vl4 = new VakLeerkracht();
            vl4.setLeerkrachtId(lk);
            vl4.setVakId(vak2);
            vl4.setKlasCollection(klassen1);        
            vakLeerkrachten.add(vl);
            vakLeerkrachten.add(vl2);
            vakLeerkrachten.add(vl3);
            vakLeerkrachten.add(vl4);        
            vakLeerkrachtService.saveVakLeerkrachten(vakLeerkrachten);

            List<Student>studenten = new ArrayList<>();
            Student student = new Student();
            student.setNaam("Lennon");
            student.setVoornaam("John");
            student.setEmail("testscoretracker@gmail.com");
            student.setKlasId(klas);
            student.setStudentnummer("r0853641");        
            List<Vak>vakken1 = new ArrayList<>();
            vakken1.add(vak);
            vakken1.add(vak2);
            student.setVakCollection(vakken1);
            Student student2 = new Student();
            student2.setNaam("Verstraeten");
            student2.setVoornaam("Danny");
            student2.setEmail("testscoretracker@gmail.com");
            student2.setKlasId(klas); 
            student2.setStudentnummer("r0703299");
            student2.setVakCollection(vakken1);
            Student student3 = new Student();
            student3.setNaam("Minogue");
            student3.setVoornaam("Kylie");
            student3.setEmail("testscoretracker@gmail.com");
            student3.setKlasId(klas2); 
            student3.setStudentnummer("r0884371");
            List<Vak>vakken2 = new ArrayList<>();
            vakken2.add(vak2);
            vakken2.add(vak3);
            student3.setVakCollection(vakken2);
            Student student4 = new Student();
            student4.setNaam("Potter");
            student4.setVoornaam("Harry");
            student4.setEmail("testscoretracker@gmail.com");
            student4.setKlasId(klas2);
            student4.setVakCollection(vakken2); 
            student4.setStudentnummer("r0145573");
            Student student5 = new Student();
            student5.setNaam("Collins");
            student5.setVoornaam("Phil");
            student5.setEmail("testscoretracker@gmail.com");
            student5.setKlasId(klas);
            student5.setStudentnummer("r0722651");    
            student5.setVakCollection(vakken1);
            Student student6 = new Student();
            student6.setNaam("Kidman");
            student6.setVoornaam("Nicole");
            student6.setEmail("testscoretracker@gmail.com");
            student6.setKlasId(klas2);
            student6.setVakCollection(vakken2); 
            student6.setStudentnummer("r0932284");
            studenten.add(student);
            studenten.add(student2);
            studenten.add(student3);
            studenten.add(student4);  
            studenten.add(student5); 
            studenten.add(student6);
            studentService.saveStudents(studenten);

            List<StudentTest>studentTesten = new ArrayList<>();        
            StudentTest st = new StudentTest();
            st.setTestId(test);
            st.setStudentId(student);
            st.setScore(75.0);             
            StudentTest st2 = new StudentTest();
            st2.setTestId(test);
            st2.setStudentId(student2);
            st2.setScore(64.8);      
            StudentTest st3 = new StudentTest();
            st3.setTestId(test3);
            st3.setStudentId(student3);
            st3.setScore(42.8);       
            StudentTest st4 = new StudentTest();
            st4.setTestId(test3);
            st4.setStudentId(student4);
            st4.setScore(98.0);        
            StudentTest st5 = new StudentTest();
            st5.setTestId(test3);
            st5.setStudentId(student6);
            st5.setScore(81.9);  
            StudentTest st6 = new StudentTest();
            st6.setTestId(test2);
            st6.setStudentId(student);
            st6.setScore(64.8);  
            StudentTest st7 = new StudentTest();
            st7.setTestId(test2);
            st7.setStudentId(student2);
            st7.setScore(78.1); 
            StudentTest st8 = new StudentTest();
            st8.setTestId(test2);
            st8.setStudentId(student5);
            st8.setScore(64.7); 
            StudentTest st9 = new StudentTest();
            st9.setTestId(test);
            st9.setStudentId(student5);
            st9.setScore(22.1); 
            StudentTest st10 = new StudentTest();
            st10.setTestId(test3);
            st10.setStudentId(student);
            st10.setScore(87.1);       
            StudentTest st11 = new StudentTest();
            st11.setTestId(test3);
            st11.setStudentId(student5);
            st11.setScore(72.0);        
            StudentTest st12 = new StudentTest();
            st12.setTestId(test3);
            st12.setStudentId(student2);
            st12.setScore(34.1); 
            StudentTest st13 = new StudentTest();
            st13.setTestId(test4);
            st13.setStudentId(student3);
            st13.setScore(24.8); 
            StudentTest st14 = new StudentTest();
            st14.setTestId(test4);
            st14.setStudentId(student4);
            st14.setScore(77.0);
            StudentTest st15 = new StudentTest();
            st15.setTestId(test4);
            st15.setStudentId(student6);
            st15.setScore(59.1); 
            StudentTest st16 = new StudentTest();
            st16.setTestId(test5);
            st16.setStudentId(student);
            st16.setScore(47.9); 
            StudentTest st17 = new StudentTest();
            st17.setTestId(test5);
            st17.setStudentId(student2);
            st17.setScore(52.1); 
            StudentTest st18 = new StudentTest();
            st18.setTestId(test5);
            st18.setStudentId(student5);
            st18.setScore(86.3); 
            StudentTest st19 = new StudentTest();
            st19.setTestId(test5);
            st19.setStudentId(student3);
            st19.setScore(59.1); 
            StudentTest st20 = new StudentTest();
            st20.setTestId(test5);
            st20.setStudentId(student4);
            st20.setScore(17.8); 
            StudentTest st21 = new StudentTest();
            st21.setTestId(test5);
            st21.setStudentId(student6);
            st21.setScore(80.4); 
            try 
            {  
                String datestr="27/04/2016";
                String datestr2="25/04/2016";
                String datestr3="10/02/2016";
                String datestr4="07/05/2016";
                String datestr5="13/03/2016";
                String datestr6="17/02/2016";
                String datestr7="01/03/2016";
                DateFormat formatter; 
                formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date date1 = (Date)formatter.parse(datestr); 
                Date date2 = (Date)formatter.parse(datestr2);
                Date date3 = (Date)formatter.parse(datestr3);
                Date date4 = (Date)formatter.parse(datestr4);
                Date date5 = (Date)formatter.parse(datestr5);
                Date date6 = (Date)formatter.parse(datestr6);
                Date date7 = (Date)formatter.parse(datestr7);
                st.setDatum(date1); 
                st2.setDatum(date1);
                st9.setDatum(date1);
                st3.setDatum(date3);            
                st4.setDatum(date3);
                st5.setDatum(date3); 
                st6.setDatum(date4);
                st7.setDatum(date4);
                st8.setDatum(date4);
                st10.setDatum(date2);
                st11.setDatum(date2);
                st12.setDatum(date2);
                st13.setDatum(date5);
                st14.setDatum(date5);
                st15.setDatum(date5);
                st16.setDatum(date6);
                st17.setDatum(date6);
                st18.setDatum(date6);
                st19.setDatum(date7);
                st20.setDatum(date7);
                st21.setDatum(date7);
            } 
                catch (Exception e)
            {}
            studentTesten.add(st);
            studentTesten.add(st2);
            studentTesten.add(st3);
            studentTesten.add(st4);
            studentTesten.add(st5);   
            studentTesten.add(st6); 
            studentTesten.add(st7); 
            studentTesten.add(st8); 
            studentTesten.add(st9); 
            studentTesten.add(st10); 
            studentTesten.add(st11); 
            studentTesten.add(st12); 
            studentTesten.add(st13); 
            studentTesten.add(st14); 
            studentTesten.add(st15); 
            studentTesten.add(st16); 
            studentTesten.add(st17); 
            studentTesten.add(st18); 
            studentTesten.add(st19); 
            studentTesten.add(st20); 
            studentTesten.add(st21); 
            studentTestService.saveStudentTesten(studentTesten);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
