
package services;

import entities.Leerkracht;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author e_for
 */
@Stateless
@Path("entities.leerkracht")
@Transactional
public class LeerkrachtFacadeREST extends AbstractFacade<Leerkracht> {

    @PersistenceContext(unitName = "be.thomasmore.java_ScoreTrackerServices_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public LeerkrachtFacadeREST() {
        super(Leerkracht.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Leerkracht entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Leerkracht entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Leerkracht find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Leerkracht> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Leerkracht> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    //Methode om een lijst van leerkrachten op te slaan in de db
    @POST
    @Path("saveLeerkrachten")
    public List<Leerkracht> saveLeerkrachten(List<Leerkracht> leerkrachten) {
        for (Leerkracht lk : leerkrachten) {
            em.persist(lk);
        }
        return leerkrachten;
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
