
package services;

import entities.Vak;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Stateless
@Path("entities.vak")
@Transactional
public class VakFacadeREST extends AbstractFacade<Vak> {

    @PersistenceContext(unitName = "be.thomasmore.java_ScoreTrackerServices_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public VakFacadeREST() {
        super(Vak.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Vak entity) {
        super.create(entity);
    }
    
    @POST
    public Long createWithId(Vak entity) {
        super.create(entity);
        return entity.getId();
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Vak entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Vak find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Vak> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Vak> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    //Methode om een lijst van vakken op te slaan in de db
    @POST
    @Path("saveVakken")
    public List<Vak> saveVakken(List<Vak> vakken) {
        for (Vak vak : vakken) {
            em.persist(vak);
        }
        return vakken;
    }
    
    //Methode om een vak uit de db op te halen adv zijn naam
    @GET
    @Path("findVakByNaam/{vakNaam}")
    public Vak findVakByNaam(@PathParam("vakNaam") String naam) {
        Vak vak;
        vak = (Vak)em.createNamedQuery("Vak.findByNaam").setParameter("naam", naam).getSingleResult();
        return vak;
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
