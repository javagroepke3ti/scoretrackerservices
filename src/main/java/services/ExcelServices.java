/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import excel.ExcelReaderImpl;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author Thomas
 */
@Stateless
@Path("excels")
public class ExcelServices {

    @EJB
    private KlasFacadeREST klasService;
    @EJB
    private StudentFacadeREST studentService;
    @EJB
    private VakFacadeREST vakService;
    @EJB
    private TestFacadeREST testService;
    @EJB
    private StudentTestFacadeREST studentTestService;

    @POST
    @Path("uploadKlassenlijst")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response storeKlaslijst(@FormDataParam("file") InputStream excelFile, @FormDataParam("file") InputStream excelFile2, @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {
        URI uri = null;
        try {
            ExcelReaderImpl instance = new ExcelReaderImpl();
            String resultaat = instance.readStudentsKlasAndSave(excelFile, studentService, klasService);
        
            switch(resultaat)
            {
                case "klasBestaatAl":
                   try {
                       uri = new URI("http://localhost:8080/ScoreTrackerClient//ExcelServlet?klasBestaatAl=Y");
                   } catch (URISyntaxException ex) {
                       Logger.getLogger(ExcelServices.class.getName()).log(Level.SEVERE, null, ex);
                   }
                   break;
                case "succes" :
                   try {
                       uri = new URI("http://localhost:8080/ScoreTrackerClient/upload_succes.jsp");
                   } catch (URISyntaxException ex) {
                       Logger.getLogger(ExcelServices.class.getName()).log(Level.SEVERE, null, ex);
                   } 
                   break;
            }
        } catch (NotOfficeXmlFileException e)
        {
            try {
                    uri = new URI("http://localhost:8080/ScoreTrackerClient/ExcelServlet?geenExcelFile=Y");
                } catch (URISyntaxException ex) {
                    Logger.getLogger(ExcelServices.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        return Response.temporaryRedirect(uri).build();
    }
    
    @POST
    @Path("uploadOpdrachtlijst")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response storeOpdrachtlijst(@FormDataParam("file") InputStream excelFile, @FormDataParam("file") InputStream excelFile2, @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {
        URI uri = null;
        try {
            ExcelReaderImpl instance = new ExcelReaderImpl();
            String resultaat = instance.readTest(excelFile, studentService, vakService, klasService, testService, studentTestService);
        
            switch (resultaat)
            {
                case "succes":
                    try {
                        uri = new URI("http://localhost:8080/ScoreTrackerClient/upload_opdracht_succes.jsp");
                    } catch (URISyntaxException ex) {
                        Logger.getLogger(ExcelServices.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case "klasBestaatNiet":
                    try {
                        uri = new URI("http://localhost:8080/ScoreTrackerClient/ExcelServlet?klasBestaatNiet=Y");
                    } catch (URISyntaxException ex) {
                        Logger.getLogger(ExcelServices.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case "testBestaatAl":
                    try {
                        uri = new URI("http://localhost:8080/ScoreTrackerClient/ExcelServlet?testBestaatAl=Y");
                    } catch (URISyntaxException ex) {
                        Logger.getLogger(ExcelServices.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case "foutStudent":
                    try {
                        uri = new URI("http://localhost:8080/ScoreTrackerClient/ExcelServlet?foutStudent=Y");
                    } catch (URISyntaxException ex) {
                        Logger.getLogger(ExcelServices.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
            }
        } catch (NotOfficeXmlFileException e)
        {
            try {
                    uri = new URI("http://localhost:8080/ScoreTrackerClient/ExcelServlet?geenExcel=Y");
                } catch (URISyntaxException ex) {
                    Logger.getLogger(ExcelServices.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        return Response.temporaryRedirect(uri).build();
    }


}
