
package services;

import entities.Klas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("entities.klas")
@Transactional
public class KlasFacadeREST extends AbstractFacade<Klas> {

    @PersistenceContext(unitName = "be.thomasmore.java_ScoreTrackerServices_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public KlasFacadeREST() {
        super(Klas.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Klas entity) {
        super.create(entity);
    }
    
    @POST
    public Long createWithId(Klas entity) {
        super.create(entity);
        return entity.getId();
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Klas entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Klas find(@PathParam("id") Long id) {
        return super.find(id);
    }
    
    @GET
    @Path("klas/{id}")
    public Klas findKlas(@PathParam("id") Long id) {
        return super.find(id);
    }

    
    @GET
    @Path("all")
    @Override
    @Produces(MediaType.APPLICATION_XML)
    public List<Klas> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Klas> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    //Methode om een lijst van klassen op te slaan in de db
    @POST
    @Path("maakMeerderKlas")
    public List<Klas> saveKlassen(List<Klas> klassen) {
        for (Klas klas : klassen) {
            em.persist(klas);
            em.flush();
        }
        return klassen;
    }
    
    //Methode om een vak uit de db op te halen adv zijn naam
    @GET
    @Path("findKlasByNaam/{klasNaam}")
    public Klas findKlasByNaam(@PathParam("klasNaam") String naam) {
        Klas klas;
        klas = (Klas)em.createNamedQuery("Klas.findByNaam").setParameter("naam", naam).getSingleResult();
        return klas;
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
