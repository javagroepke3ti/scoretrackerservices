
package services;

import entities.StudentTest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Stateless
@Path("entities.studenttest")
@Transactional
public class StudentTestFacadeREST extends AbstractFacade<StudentTest> {
    
    StudentTest st = new StudentTest();

    @PersistenceContext(unitName = "be.thomasmore.java_ScoreTrackerServices_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public StudentTestFacadeREST() {
        super(StudentTest.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(StudentTest entity) {
        super.create(entity);
    }
    
    @POST
    public Long createWithId(StudentTest entity) {
        super.create(entity);
        return entity.getId();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_XML)
    public void edit(@PathParam("id") Long id, StudentTest entity) {
        super.edit(entity);
    }
    
    @GET
    @Path("/updateTestScore/{id}/{score}")
    @Produces(MediaType.APPLICATION_XML)
    public StudentTest findAndUpdate(@PathParam("id") Long id,@PathParam("score") String score) {
        StudentTest studentTest = super.find(id);
        studentTest.setScore(Double.parseDouble(score));
        super.edit(studentTest);
        
        return studentTest;
    }
    
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_XML)
    public StudentTest find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<StudentTest> findAll() {
        return super.findAll();
    }
    
    //AANGEPAST!!
    @GET
    @Path("allPerKlas/{klasId}")
    @Produces(MediaType.APPLICATION_XML)
    public List<StudentTest> findAllPerKlas(@PathParam ("klasId") Long klasId) {
        Query q = em.createQuery("select s from StudentTest s where s.studentId.klasId.id = :klasId");
        q.setParameter("klasId", klasId);
        List<StudentTest> testen= q.getResultList();
        return testen;
    }
    
     //AANGEPAST!!
    @GET
    @Path("allPerKlasPerTest/{klasId}/{testId}")
    @Produces(MediaType.APPLICATION_XML)
    public List<StudentTest> findAllPerKlasPerTest(@PathParam ("klasId") Long klasId, @PathParam ("testId") Long testId) {
        Query q = em.createQuery("select s from StudentTest s where s.studentId.klasId.id = :klasId and s.testId.id = :testId");
        q.setParameter("klasId", klasId);
        q.setParameter("testId", testId);
        List<StudentTest> testen= q.getResultList();
        return testen;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<StudentTest> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    //AANGEPAST!!
    @GET
    @Path("allTestsFromStudentByVakByDate/{studentId}/{vakId}/{datum1}/{datum2})")
    public List<StudentTest> findAllTestsPerStudentByVakAndDate(@PathParam("studentId") Long studentId, @PathParam("vakId") Long vakId, @PathParam("datum1") Date datum1, @PathParam("datum2") Date datum2)
    {
        Query q = em.createQuery("select s from StudentTest s where s.studentId.id = :studentId and s.testId.vakId.id = :vakId and s.datum between :datum1 and :datum2 order by s.datum");
        q.setParameter("studentId", studentId);
        q.setParameter("vakId", vakId);
        q.setParameter("datum1", datum1);
        q.setParameter("datum2", datum2);
        List<StudentTest> studentTesten = q.getResultList();
        
        return studentTesten;
    }
    
    //AANGEPAST!!
    @GET
    @Path("allPerStudent/{studentId}/{startDatum}/{eindDatum}")
    @Produces(MediaType.APPLICATION_XML)
    public List<StudentTest> findAllTestsPerStudentByDate(@PathParam("studentId") Long studentId, @PathParam("startDatum") String startDatum, @PathParam("eindDatum") String eindDatum) throws ParseException
    {
        String[] data1 = startDatum.split("-");
        String datum1 = data1[0] + "/" + data1[1] + "/" + data1[2];
        String[] data2 = eindDatum.split("-");
        String datum2 = data2[0] + "/" + data2[1] + "/" + data2[2];
        DateFormat formatter; 
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = (java.util.Date)formatter.parse(datum1); 
        Date date2 = (java.util.Date)formatter.parse(datum2);
        
        Query q = em.createQuery("select s from StudentTest s where s.studentId.id = :studentId and s.datum between :datum1 and :datum2");
        q.setParameter("studentId", studentId);
        q.setParameter("datum1", date1);
        q.setParameter("datum2", date2);
        List<StudentTest> studentTesten = q.getResultList();
        
        return studentTesten;
    }
    
    //AANGEPAST!!
    @GET
    @Path("allTestsFromKlasByVakByDate/{klasId}/{vakId}/{datum1}/{datum2})")
    public List<StudentTest> findAllTestsFromKlasByVakAndDate(@PathParam("studentId") Long klasId, @PathParam("vakId") Long vakId, @PathParam("datum1") Date datum1, @PathParam("datum2") Date datum2)
    {
        Query q = em.createQuery("select s from StudentTest s where s.studentId.klasId.id = :klasId and s.testId.vakId.id = :vakId and s.datum between :datum1 and :datum2 order by s.datum");
        q.setParameter("klasId", klasId);
        q.setParameter("vakId", vakId);
        q.setParameter("datum1", datum1);
        q.setParameter("datum2", datum2);
        List<StudentTest> studentTesten = q.getResultList();
        
        return studentTesten;
    }
    
    //Methode om een lijst van studentTesten op te slaan in de db
    public List<StudentTest> saveStudentTesten(List<StudentTest> studentTesten) {
        for (StudentTest studentTest : studentTesten) {
            em.persist(studentTest);
        }
        return studentTesten;
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
