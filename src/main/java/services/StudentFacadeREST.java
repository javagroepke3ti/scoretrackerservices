
package services;

import entities.Student;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Stateless
@Path("entities.student")
@Transactional
public class StudentFacadeREST extends AbstractFacade<Student> {

    @PersistenceContext(unitName = "be.thomasmore.java_ScoreTrackerServices_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public StudentFacadeREST() {
        super(Student.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Student entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Student entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Student find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Student> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Student> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
   
    @GET
    @Path("findAllPerKlas/{klasId}")
    @Produces(MediaType.APPLICATION_XML)
    public List<Student> findAllPerKlas(@PathParam("klasId") Long klasId) {
        Query q = em.createQuery("select s from Student s where s.klasId.id = :klasId order by s.naam");
        q.setParameter("klasId", klasId);
        List<Student> studenten = q.getResultList();
        return studenten;
    }

    //Methode om een lijst van studenten op te slaan in de db
    @POST
    @Path("save")
    public List<Student> saveStudents(List<Student> studenten) {
        for (Student student : studenten) {
            em.persist(student);
        }
        return studenten;
    }
    
       //Methode om een student uit de db op te halen adv zijn naam
    @GET
    @Path("findStudentByNaam/{studentNaam}")
    public Student findStudentByNaam(@PathParam("studentNaam") String naam) {
        Student student;
        student = (Student) em.createNamedQuery("Student.findByNaam").setParameter("naam", naam).getSingleResult();
        return student;
    }
    
    @GET
    @Path("findStudentByNummer/{studentNummer}")
    public Student findStudentByNummer(@PathParam("studentNummer") String nummer) {
        Student student;
        student = (Student) em.createNamedQuery("Student.findByStudentnummer").setParameter("studentnummer", nummer).getSingleResult();
        return student;
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
