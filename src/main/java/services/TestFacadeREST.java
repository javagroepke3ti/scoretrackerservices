
package services;

import entities.Test;
import java.util.HashSet;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("entities.test")
@Transactional
public class TestFacadeREST extends AbstractFacade<Test> {

    @PersistenceContext(unitName = "be.thomasmore.java_ScoreTrackerServices_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public TestFacadeREST() {
        super(Test.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Test entity) {
        super.create(entity);
    }
    
    @POST
    public Long createWithId(Test entity) {
        super.create(entity);
        return entity.getId();
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Test entity) {
        super.edit(entity);
    }
    
    public void update(Test entity) {
        super.edit(entity);
    }
    
    public Long updateWithId(Test entity) {
        super.edit(entity);
        return entity.getId();
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Test find(@PathParam("id") Long id) {
        return super.find(id);
    }
    
    @GET
    @Path("test/{id}")
    public Test findTest(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Test> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Test> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    //Methode om een lijst van testen op te slaan in de db
    @POST
    @Path("saveTesten")
    public List<Test> saveTesten(List<Test> testen) {
        for (Test test : testen) {
            em.persist(test);
            em.flush();           
        }
        return testen;
    }
    
     //Methode om een vak uit de db op te halen adv zijn naam
    @GET
    @Path("findTestByNaam/{testNaam}")
    public List<Test> findTestByNaam(@PathParam("testNaam") String naam) {
        List<Test> testen = em.createNamedQuery("Test.findByNaam").setParameter("naam", naam).getResultList();
        return testen;
    }
    
    //Methode om een vak uit de db op te halen adv zijn naam en vak
    @GET
    @Path("findTestByNaamEnVak/{testNaam}/{vakNaam}")
    public Test findTestByNaamEnVak(@PathParam("testNaam") String naam, @PathParam("vakNaam") String vakNaam) {
        Test test = (Test)em.createNamedQuery("Test.findByNaamEnVak").setParameter("naam", naam).setParameter("vaknaam", vakNaam).getSingleResult();
        return test;
    }
   

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
