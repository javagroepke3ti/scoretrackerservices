/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Klas;
import entities.Student;
import entities.Test;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import pdf.PdfCreatorImpl;

/**
 *
 * @author Thomas
 */
@Stateless
@Path("testen")
public class DownloadTestenPdfService {
    
    
    @EJB
    private EmailService mailService;
    @EJB
    private KlasFacadeREST klasService; 
    @EJB
    private TestFacadeREST testService;     
    @EJB
    private StudentFacadeREST studentService;
    @EJB
    private StudentTestFacadeREST studentTestService;

    @GET
    @Path("/create/{klasId}/{testId}")
    @Produces("application/pdf")
    public Response createPdf(@PathParam("klasId") Long klasId, @PathParam("testId") Long testId) throws IOException {
        Klas klas = klasService.find(klasId);
        Test gekozenTest = testService.find(testId);
        
        PdfCreatorImpl instance = new PdfCreatorImpl();
        Response response = instance.createPdf(klas, gekozenTest, studentTestService);
        return response;
    }
    
    @GET
    @Path("/createDates/{studentId}/{datum1}/{datum2}")
    @Produces("application/pdf")
    public Response createPdfWithDates(@PathParam("studentId") Long studentId, @PathParam("datum1")String startDatum, @PathParam("datum2") String eindDatum) throws IOException, ParseException {
        String[] data1 = startDatum.split("-");
        String datum1 = data1[0] + "/" + data1[1] + "/" + data1[2];
        String[] data2 = eindDatum.split("-");
        String datum2 = data2[0] + "/" + data2[1] + "/" + data2[2];
        
        Student student = studentService.find(studentId);
        PdfCreatorImpl instance = new PdfCreatorImpl();
        Response response = instance.createPdfWithDates(student, datum1, datum2, studentTestService);
        return response;
    }
    
    @GET
    @Path("/createDatesForStudent/{studentId}/{datum1}/{datum2}")
    @Produces("application/pdf")
    public Response createPdfWithDatesForStudent(@PathParam("studentId") Long studentId, @PathParam("datum1")String startDatum, @PathParam("datum2") String eindDatum) throws IOException, ParseException {
        String[] data1 = startDatum.split("-");
        String datum1 = data1[0] + "/" + data1[1] + "/" + data1[2];
        String[] data2 = eindDatum.split("-");
        String datum2 = data2[0] + "/" + data2[1] + "/" + data2[2];
        
        Student student = studentService.find(studentId);
        PdfCreatorImpl instance = new PdfCreatorImpl();
        instance.createPdfWithDatesForStudent(student, datum1, datum2, studentTestService, mailService);
        
        URI uri = null;
        try {
                    uri = new URI("http://localhost:8080/ScoreTrackerClient/MailServlet?student=" + studentId);
                } catch (URISyntaxException ex) {
                    Logger.getLogger(ExcelServices.class.getName()).log(Level.SEVERE, null, ex);
                }
        return Response.temporaryRedirect(uri).build();
    }
}
